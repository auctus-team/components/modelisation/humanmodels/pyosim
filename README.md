# PyOsim 

A simple python3 wrapper of the OpenSim library. 

## Dependencies
There are two dependencies: `numpy` and of course `OpenSim`. Numpy you can install easily using pip:
```
pip install numpy 
# or for python3
pip3 install numpy
# or in conda 
conda install numpy
```
The prefered python version is python3 but it will work with python2 as well. The constraint will be the opensim installation.


## How to use it
Clone the github repository to your project: 

```
cd my_project
git clone git@gitlab.inria.fr:human/pyosim.git
```

Your project structure will be something like this:
```
my_project
  - my_code.py
  - ...
  - pyosim
        - OsimModel.py
        - algebra.py
        - __init__.py
        - arm26.osim
```

And then in the code you just import
```python
from pyosim.OsimMode import OsimModel 
```

## Import into an other project 

(from https://docs.ros.org/en/api/catkin/html/howto/format2/installing_python.html)

You can do it with a CMakeLists.txt, into a catkin package.
Just add the following line into your CMakeLists.txt : 

~~~
catkin_python_setup()
~~~

Then, build your package, source devel/setup.bash, and you will have access to the pyosim module : 

~~~
from pyosim.OsimMode import OsimModel 
~~~

A example is given into the human_library package, with the demo_pyosim example.

## Code Docs
Find the full docs on the [link](./docs/README.md)

## Examples

### Display a model
Minimal code for displaying the opensim model
```python
from pyosim.OsimModel import OsimModel

## model path
model_path = 'your path to the model model.osim'

## Construct and display the model
OsimModel(model_path,visualize=True).displayModel()
```


### Display model states
Minimal code for displaying the opensim model
```python
from pyosim.OsimModel import OsimModel

model_path = 'your path to the model model.osim'
model = OsimModel(model_path,visualize=True)
# set model to neutral or random state
model.setToRandom()
# model.setToNeutral()

# print different model variables
model.printBodiesPositions()
model.printMarkersPositions()
model.printJointsValues()

# get model joint limits
print(model.getJointLimits())
# get muscle force limits
print(model.getForceLimits())

# print model jacobian
print(model.getJacobian(n=11))

# display model
model.displayModel()

```

## Examples
Find mode code examples at: https://gitlab.inria.fr/askuric/pyosim_examples
