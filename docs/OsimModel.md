<!-- markdownlint-disable -->

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L0"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

# <kbd>module</kbd> `OsimModel`
Created on Wed Dec 16 13:31:14 2020 

@author: elandais 



---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L20"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>class</kbd> `OsimModel`
! Class creating a Osim Model, saving and updating useful informations about it. 

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L24"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `__init__`

```python
__init__(
    model_path,
    geometry_path='',
    ik_path='',
    setup_ik_file='',
    trc_path='',
    visualize=False
)
```

! 

Constructor of the OsimModel class. 



**Args:**
 
 - <b>`model_path`</b> (String): Absolute path to the .osim file (with the .osim filename). 


 - <b>`geometry_path`</b> (String, optional): Path to the .vtp files associated with the .osim model. 


 - <b>`ik_path`</b> (String, optional):  Path to the Inverse Kinematics configuration file. The default is "". 


 - <b>`setup_ik_file`</b> (String, optional): Name of the Inverse Kinematics configuration file. This file is used  to set parameters of the Inverse Kinematics operation (weight of each  markers of the model for example). The default is "". 
 - <b>`trc_path`</b> (String, optional): Path to a trc file associated with the .osim file. This file contains  the different positions of each one of the markers of the model, at  each time. This file is then used to determine the position of each  one of the joints of the model at each time. The default is "".  
 - <b>`visualize `</b>:  Boolean, optional  Activate the visualization of the model. The default is False. 




---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L1204"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `IK_from_markers_positions`

```python
IK_from_markers_positions(markers_positions, xml_taskset, trc_path)
```

Method calculating inverse kinematics from marker positions (ask Erwann for info) 

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L477"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `displayModel`

```python
displayModel(trc_path=None, compl_ik_path=None)
```

! 

(BETA). Method used to display a .osim Model.             



**Args:**
 
 - <b>`osimModel `</b>:  opensim.simulation.Model.   Object describing a .osim model, from the opensim module. 


 - <b>`osimState `</b>:  opensim.simbody.State.  Object describing the state of a .osim model, from the opensim module.  


 - <b>`trc_path `</b>:  String  Path to a trc file associated with the .osim file. This file contains  the different positions of each one of the markers of the model, at  each time. This file is then used to determine the position of each  one of the joints of the model at each time. 

 


 - <b>`compl_ik_path `</b>:  String.  Complete path to the Inverse Kinematics configuration file.  

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L822"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `fixScalingFactors`

```python
fixScalingFactors(attribuedBodies)
```

Method for adapting scaling factors, (ask Erwann for info) 

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L741"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `getBodiesDistancesParentChild`

```python
getBodiesDistancesParentChild()
```

Method for calculating the distance in between parent and child 



**Returns:**
 
 - <b>`D_body_index `</b>:  indexes of bodies 
 - <b>`D_body_parent_child_distances `</b>:  distance per body - body : [ [index_parent, dist], [ [index_child_i, dist_i] ]  ] 

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L628"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `getBodiesPositions`

```python
getBodiesPositions()
```

! 

Method used to get the positions of the body parts of the model, expressed into the global referential Ground. 



**Returns:**
 
 - <b>`D_body_pos `</b>:  Dictionary of list. Dictionary expressing, for each body part, its position and rotation matrix into the Ground referential. 

Example :   D_body_pos[hand_l] = [ M_p, M_R ]  M_p = np.array([[0.5],  [0.5],  [0.75]])  M_R = np.eye(3,3) 

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L342"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `getCorresJointsCoord`

```python
getCorresJointsCoord()
```

! 

Method used to get a quick access to the correspondances between the joints of the model and the coordinates associated with each one of those joints. 

A coordinate is associated to an unique joint, but a joint can have multiple coordinates.  



**Returns:**
 
 - <b>`D_coord_joints `</b>:  Dictionary.  Dictionary linking the names of the coordinates objects to the name of  their respective joints. 
 - <b>`Ex `</b>:  D_coord_joints[pelvis_tx] = "pelvis"  D_coord_joints[pelvis_ty] = "pelvis"  D_coord_joints[pelvis_tz] = "pelvis"  D_coord_joints[pelvis_rx] = "pelvis"  D_coord_joints[pelvis_ry] = "pelvis"  D_coord_joints[pelvis_rz] = "pelvis" 


 - <b>`D_joints_coord `</b>:  Dictionary.   Dictionary linking the names of the joints to the names of the coordinates  objects associated with these joints.  
 - <b>`Ex `</b>:  D_joints_coord[pelvis] = [pelvis_tx,pelvis_ty,pelvis_tz,pelvis_rx,pelvis_ry,pelvis_rz] 

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L251"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `getCorresQCoord`

```python
getCorresQCoord()
```

! 

Method used to get the correspondance between the index associated with the value of the joints of the model, for the variable coordinateSet  and the variable Q. 



**Returns:**
 
 - <b>`D_Q_coord `</b>:  Dictionary.     D_Q_coord is organised as follows :   D_Q_coord[i] = j <--->  Q[i] = coordinateSet[j].getValue(osimState).  

 The value of coordinate j (wrt CoordinateSet) corresponds to the i-th component  of Q from osimState. 

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L1514"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `getForceLimits`

```python
getForceLimits()
```

! 

Method returning the current maximal force limits for each muscle. Upper limit is the max  isometric force and lower limit is the passive force applied 

Returns  F_min: list  List of minimal forces 

 F_max: list  List of maximal forces 

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L193"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `getGroundProperties`

```python
getGroundProperties()
```

! 

Method used to gather some useful properties about the Ground link of the .osim model.  



**Returns:**
 
 - <b>`D_gd_prop `</b>:  Dictionary of list.  Dictionary containing, for each of the coordinates (= DOF) names of the   Ground joint, the type and the axis associated with this coordinate. 

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L1336"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `getJacobian`

```python
getJacobian(v=[0, 0, 0], n=0)
```

! 

Return a 6xNQ frame task Jacobian, for a frame task A fixed to a body B designated by an) index n.  NQ is the size of the vector Q. The origin of the frame task A into the referential B is expressed by the vector v. 



**Args:**
 
 - <b>`v `</b>:  opensim.simbody.Vec3, optional.  Origin of the frame task A into the referential B. The default is osim.Vec3().  


 - <b>`n `</b>:  Int, optional.  Mobilized Body Index. The body index to which the frame of interest  is fixed. The default is 0 

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L308"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `getJointsLimits`

```python
getJointsLimits()
```

! 

Method used to get the limits values of each one of the DOF of the model. 



**Returns:**
 
 - <b>`D_joints_range `</b>:  Dictionary of List  D_joints_range is organised as follows :   D_joints_range[i] = [lower limit of DOF i, upper limit of DOF i]. 

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L411"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `getJointsPositions`

```python
getJointsPositions(refe='Ground', n=1)
```

! Method used to get the positions of one of the two frames of the joints of the model. The positions of the selected frames can then be expressed into  the global referential "Ground" of the model, or the referential of the parent joint "Parent" of the selected joint. 



**Args:**
 
 - <b>`refe `</b>:  String, optional. 

 Type of referential chosen to express the joints position. Two choices  are available :   

 refe = "Ground", to express the positions into the global referential   Ground.   

 refe = "Parent", to express the positions into the referential of the parent  joint of the selected joint. This could be useful to determine the position   of the joint compared to the previous joint.  

 The default is "Ground". 


 - <b>`n `</b>:  Int, optional. 

 Value associated with the joint frame to be selected for each of the   joints. 
 - <b>`Two frames are available `</b>:  one frame fixed in relation to the parent joint,  the other fixed in relation to the current joint.  


 - <b>`n = 0 `</b>:  frame fixed in relation to the parent joint. 


 - <b>`n = 1 `</b>:  frame fixed in relation to the current joint. 

The default is 1. 



**Returns:**
 
 - <b>`D_joints_pos `</b>:  Dictionary of list.  Dictionary expressing, for each joint, its position and rotation matrix   in the chosen referential.   

 Example :   D_joints_pos[pelvis] = [ M_p, M_R ]  M_p = np.array([[0.],  [2.],  [0.]])  M_R = np.eye(3,3) 

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L664"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `getJointsValues`

```python
getJointsValues()
```

! 

Method used to get, for each coordinate object of the .osim model, its value. 



**Returns:**
 
 - <b>`D_joint_value `</b>:  Dictionary. Dictionary expressing, for each coordinate object, its value. Example :   D_joint_value[pelvis_ty] = 2.0 

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L712"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `getMarkersByBodyParts`

```python
getMarkersByBodyParts()
```

! 

Method used to get dictionary of markers per body part 



**Returns:**
 
 - <b>`D_body_markers`</b>:  Dictionary of markers with body pars as keys 

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L1299"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `getMarkersPositions`

```python
getMarkersPositions()
```





---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L1308"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `getMarkersPositionsInLocalReferential`

```python
getMarkersPositionsInLocalReferential()
```





---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L1465"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `getMomentArm`

```python
getMomentArm(indDOF=None, indMuscles=None)
```

Moment arm matrix calculation (translated from Matlab code created by Nasser) 



**Args:**
 
 - <b>`indDOF`</b>:  indexes of used joints (DOF) of the model (by default all used) 
 - <b>`indMuscles`</b>:  indexes ofmuslces of the model (by default all used)  



**Returns:**
 
 - <b>`MomentArm`</b>:  matrix of indDOF x indMuscles elements  

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L1168"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `ikFromFile`

```python
ikFromFile(trc_path, compl_ik_path, mot_path)
```

! 

Method calculating inverse kinematics from file 



**Args:**
 
 - <b>`trc_path `</b>:  String  Path to a trc file associated with the .osim file. This file contains  the different positions of each one of the markers of the model, at  each time. This file is then used to determine the position of each  one of the joints of the model at each time. 
 - <b>`compl_ik_path `</b>:  String.  Complete path to the Inverse Kinematics configuration file.  
 - <b>`mot_path `</b>:  String  Path to the output motion file of Inverse Kinematics. 

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L1539"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `numpy`

```python
numpy(matrix)
```

! 

Method used to transform OpenSim Mat33 or Vec into numpy objects. 



**Args:**
 
 - <b>`matrix `</b>:  opensim.simbody.Mat33 or opensim.simbody.Vec.  Input Opensim matrix. 

**Returns:**
 
 - <b>`M `</b>:  numpy.array.  Output matrix, as array. 

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L613"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `printBodiesPositions`

```python
printBodiesPositions()
```

! Method used to get the positions of the body parts of the model into the ground referential. 

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L289"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `printCorresQCoord`

```python
printCorresQCoord()
```

! 

Method used to print the correspondance between the index associated with the value of the joints of the model, for the variable coordinateSet  and the variable Q. This method is based on the method getCorresQCoord. 

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L684"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `printFramesNumber`

```python
printFramesNumber()
```

! 

Method used to print the index and the name of each frame associated  with the model. 

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L396"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `printJointsDivisionsIntoOneDOF`

```python
printJointsDivisionsIntoOneDOF()
```

! 

Method used to print the correspondances between the joints of the model and the coordinates associated with each one of those joints. Based on the method getCorresJointsCoord. 

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L328"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `printJointsLimits`

```python
printJointsLimits()
```

! 

Method used to print the limits values of each one of the DOF of the model, based on the method getJointsLimits 

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L560"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `printJointsPositions`

```python
printJointsPositions(refe='Ground', n=1)
```

! Method used to print the positions of one of the two frames of the joints of the model, into a chosen referential. Based on the method getJointsPositions. 



**Args:**
 
 - <b>`refe `</b>:  String, optional. 

 Type of referential chosen to express the joints position. Two choices  are available :   

 refe = "Ground", to express the positions into the global referential   Ground.   

 refe = "Parent", to express the positions into the referential of the parent  joint of the selected joint. This could be useful to determine the position   of the joint compared to the previous joint.  

 The default is "Ground". 


 - <b>`n `</b>:  Int, optional. 

 Value associated with the joint frame to be selected for each of the   joints. 
 - <b>`Two frames are available `</b>:  one frame fixed in relation to the parent joint,  the other fixed in relation to the current joint.  


 - <b>`n = 0 `</b>:  frame fixed in relation to the parent joint. 


 - <b>`n = 1 `</b>:  frame fixed in relation to the current joint. 

The default is 1. 

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L509"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `printJointsPositionsAllFrames`

```python
printJointsPositionsAllFrames(refe='Ground')
```

! 

Method used to print the positions of all the frames of each joint, into a  chosen referential "refe". 



**Args:**
 
 - <b>`refe `</b>:  String, optional. 

 Type of referential chosen to express the joints position. Two choices  are available :   

 refe = "Ground", to express the positions into the global referential   Ground.  

 refe = "Parent", to express the positions into the referential of the parent  joint of the selected joint. This could be useful to determine the position   of the joint compared to the previous joint.  

 The default is "Ground". 

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L699"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `printJointsValues`

```python
printJointsValues()
```

! 

Method used to print the value of each of the coordinates of the model. 

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L1317"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `printMarkersPositions`

```python
printMarkersPositions(nameToo=False)
```





---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L870"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `reassembleByAxis`

```python
reassembleByAxis(D_body_markers, parallal_val=0.8)
```

For each body part, create (ask Erwann for info) 

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L129"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `runIK`

```python
runIK()
```

! 

Method used to execute the Inverse Kinematics operation. The variables  motion_file (=path to the output motion file of Inverse Kinematics),  compl_ik_path (= path to the Inverse Kinematics configuration file) and  trc_path (= path to the marker file used by Inverse Kinematics) should  be defined first.  Those variables are defined at the constructor step if the variables ik_path, setup_ik_file and trc_path were defined. 

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L947"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `scaling`

```python
scaling(dirPath, osimFile, scaleFile, trcFile, motFile, mass, scalingFactors)
```

Inspired from https://github.com/HernandezVincent/OpenSim (ask Erwann for info) 

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L1445"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `setJointValues`

```python
setJointValues(q)
```

! 

Method used to set to random values all of the values of the articulations of the system. 



**Args:**
 
 - <b>`q `</b>:  list  List of joint values, only the unconstrained joints 

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L151"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `setQ`

```python
setQ(LQ, LQtype=0)
```

! 

Set the Q vector of the .osim model, containing the values of each joint of this model. Those values are set in accordance to a list of variables LQ.  



**Args:**
 
 - <b>`LQ `</b>:  List.  List containing the values of each joint of the model.  


 - <b>`LQtype `</b>:  Int.  Define the configuration of the LQ list.  


 - <b>`Type 0 `</b>:  Configuration according to the order of the CoordinateSet variable of the .osim Model. LQ[i] corresponds to the desired value of CoordinateSet[i]. As there is no direct link between CoordinateSet[i] and Q[i], a transformation is necessary. 


 - <b>`Type 1 `</b>:  Configuration according to the order of Q vector used by  osimState. LQ[i] corresponds to the desired value of Q[i] used by osimState.  Here, no transformation is necessary.             

The default is 0. 

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L1412"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `setToNeutral`

```python
setToNeutral()
```

! 

Method used to set to neutral value all of the values of the articulations of the system. 

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L1428"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `setToRandom`

```python
setToRandom()
```

! 

Method used to set to random values all of the values of the articulations of the system. 

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L1364"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `tryForwardTool`

```python
tryForwardTool()
```

! 

(BETA). Method used to try the Forward Tool of Opensim. (ask Erwann for info) 



**Args:**
 
 - <b>`osimModel `</b>:  opensim.simulation.Model.   Object describing a .osim model, from the opensim module. 

---

<a href="https://gitlab.inria.fr/Hanoi/pyosim/OsimModel.py#L118"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

### <kbd>method</kbd> `updModel`

```python
updModel()
```

! 

Method used to update the different variables of the OsimModel class. This method should be called after each operation on the state of the .osim model. 




---

_This file was automatically generated via [lazydocs](https://github.com/ml-tooling/lazydocs)._
